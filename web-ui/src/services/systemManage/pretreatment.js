import {BASE_URL} from '@/services/api'
import {METHOD, request} from '@/utils/request'

export async function queryAllPreItem(payload) {
  return request(`${BASE_URL}/preItem`, METHOD.GET, payload)
}

export async function addPreItem(payload) {
  return request(`${BASE_URL}/preItem`, METHOD.POST, payload)
}

export async function deleteById(payload) {
  return request(`${BASE_URL}/preItem/deleteById/${payload}`, METHOD.DELETE)
}

export async function update(payload) {
  return request(`${BASE_URL}/preItem`, METHOD.PUT, payload)
}
