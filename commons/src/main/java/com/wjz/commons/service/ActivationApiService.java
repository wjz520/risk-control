package com.wjz.commons.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wjz.commons.entity.vo.ActivationVo;
import com.wjz.commons.mapper.ActivationVoMapper;
import org.springframework.stereotype.Service;

/**
 * @author wjz
 * @date 2021/9/7 17:08
 */
@Service
public class ActivationApiService extends ServiceImpl<ActivationVoMapper, ActivationVo> {
}
