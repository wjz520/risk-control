import {BASE_URL} from '@/services/api'
import {METHOD, request} from '@/utils/request'

export async function queryAllAbstraction(payload) {
  return request(`${BASE_URL}/abstraction`, METHOD.GET, payload)
}

export async function addAbstraction(payload) {
  return request(`${BASE_URL}/abstraction`, METHOD.POST, payload)
}

export async function deleteById(payload) {
  return request(`${BASE_URL}/abstraction/deleteById/${payload}`, METHOD.DELETE)
}

export async function update(payload) {
  return request(`${BASE_URL}/abstraction`, METHOD.PUT, payload)
}
