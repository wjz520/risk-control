package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2021/7/18 14:58
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "PreItemVo", description = "预处理实体")
public class PreItemVo implements Serializable {

    @TableId
    private Long objectId;

    private Long modelId;

    private String sourceField;

    private String sourceLabel;

    private String destField;

    private String label;

    private String plugin;

    private String reqType = "GET";

    private String configJson;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private String args;
}
