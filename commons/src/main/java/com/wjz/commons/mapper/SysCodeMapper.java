package com.wjz.commons.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wjz.commons.entity.vo.SysCode;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wjz
 * @since 2020-12-22
 */
@Mapper
public interface SysCodeMapper extends BaseMapper<SysCode> {

}
