package com.wjz.engine.controller;

import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.bo.EventRequest;
import com.wjz.commons.util.ResultTool;
import com.wjz.engine.service.EngineService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author wjz
 * @date 2021/9/12 15:30
 */
@RestController
@RequestMapping("/engine")
public class EngineController {

    @Autowired
    private EngineService engineService;

    @PostMapping("/upload")
    @ApiOperation(value = "事件数据提交接口")
    public AjaxResult upload(@Valid @RequestBody EventRequest request) {

        if (request.getJsonInfo() == null || request.getReqId() == null || request.getGuid() == null) {
            return ResultTool.fail("参数不能为空");
        }
        return engineService.uploadInfo(request.getGuid(), request.getReqId(), request.getJsonInfo());
    }
}
