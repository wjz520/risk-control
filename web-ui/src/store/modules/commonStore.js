import {message} from 'ant-design-vue'

/**
 * 解析为脚本
 * @param obj
 * @returns {string}
 */
export function complexParse(obj) {
  let result = undefined
  let arr = obj.conditions
  for (let arrElement of arr) {
    if (arrElement.class == 'complex') {
      result = result + '&&' + complexParse(arrElement)
    } else {
      if (!result) {
        result = simpleParse(arrElement.expressions)
      } else {
        result = result + '&&' + simpleParse(arrElement.expressions)
      }
    }
  }
  return '(' + result + ')'
}

function simpleParse(obj) {
  return 'data.' + obj.left + obj.center + obj.right
}

//异步请求后统一弹出框
export function storePrompt(obj) {
  if (obj.success) {
    message.success('操作成功！', 3)
  } else {
    let info = '操作失败！'
    if (obj.data) {
      info += obj.data
    }
    message.error(info, 3)
  }
}

/**
 * 设置vuex的默认方法，向Basis中添加对象即可在所有vuex实例中使用
 * @param obj
 * @returns {*}
 */
const Basis = {
  mutations: {
    setState(state, payload) {
      let {name, value} = payload
      state[name] = value
    }
  },
}

export function merge(obj) {

  for (let index in obj) {
    let data = obj[index]
    for (let basis in Basis) {
      if (data[basis]) {
        Object.assign(obj[index][basis], Basis[basis])
      } else {
        Object.assign(obj[index], {[basis]: Basis[basis]})
      }
    }
  }
  return obj
}
