package com.wjz.commons.util;

import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.enums.ResultCode;

/**
 * @author wjz
 * @date 2020/11/23 16:18
 * @Description:
 */
public class ResultTool {

    public static AjaxResult success() {

        return new AjaxResult(true);
    }

    public static <T> AjaxResult<T> success(T data) {

        return new AjaxResult(true, data);
    }

    public static AjaxResult fail() {

        return new AjaxResult(false);
    }

    public static AjaxResult fail(ResultCode resultEnum) {

        return new AjaxResult(false, resultEnum);
    }

    public static <T> AjaxResult<T> fail(T data) {

        return new AjaxResult(false, data);
    }

    public static <T> AjaxResult<T> fail(T data, ResultCode resultEnum) {

        return new AjaxResult(false, resultEnum, data);
    }

}
