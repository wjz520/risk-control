package com.wjz.commons.entity.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wjz
 * @date 2021/9/7 17:05
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ActivationVo", description = "策略实体")
public class ActivationVo implements Serializable {

    @TableId
    private Long objectId;

    private String activationName;

    private Long modelId;

    private String label;

    private String comment;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private Integer bottom;

    private Integer median;

    private Integer high;

    private String ruleOrder;
}
