import {BASE_URL} from '@/services/api'
import {METHOD, request} from '@/utils/request'

export async function queryAllRule(payload) {
  return request(`${BASE_URL}/rule`, METHOD.GET, payload)
}

export async function disable(payload) {
  return request(`${BASE_URL}/rule/disable`, METHOD.GET, payload)
}

export async function addRule(payload) {
  return request(`${BASE_URL}/rule`, METHOD.POST, payload)
}

export async function deleteById(payload) {
  return request(`${BASE_URL}/rule/deleteById/${payload}`, METHOD.DELETE)
}

export async function update(payload) {
  return request(`${BASE_URL}/rule`, METHOD.PUT, payload)
}
