package com.wjz.admin.controller;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.vo.ActivationVo;
import com.wjz.commons.enums.MasterConstants;
import com.wjz.commons.service.ActivationApiService;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * @author wjz
 * @date 2021/9/7 17:09
 */
@RestController
@RequestMapping("/activation")
public class ActivationApiController {

    @Autowired
    private ActivationApiService activationApiService;

    @PostMapping
    public AjaxResult save(@RequestBody ActivationVo activationVo) {
        Long objectId = IdUtil.getSnowflake().nextId();
        activationVo.setObjectId(objectId)
                .setStatus(MasterConstants.OPEN)
                .setActivationName("activation_" + objectId);
        return ResultTool.success(activationApiService.save(activationVo.setCreateTime(new Date())));
    }

    @PutMapping
    public AjaxResult update(@RequestBody ActivationVo activationVo) {
        return ResultTool.success(activationApiService.updateById(activationVo.setUpdateTime(new Date())));
    }

    @GetMapping
    public AjaxResult queryAllActivation(Long modelId) {
        return ResultTool.success(activationApiService.list(new LambdaQueryWrapper<ActivationVo>().eq(ActivationVo::getModelId, modelId)));
    }

    @DeleteMapping("/deleteById/{objectId}")
    public AjaxResult deleteById(@PathVariable Long objectId) {
        return ResultTool.success(activationApiService.removeById(objectId));
    }

    @GetMapping("/disable")
    public AjaxResult disable(Long objectId, Integer status) {
        activationApiService.updateById(new ActivationVo().setObjectId(objectId)
                .setStatus(status == 1 ? MasterConstants.CLOSE : MasterConstants.OPEN));
        return ResultTool.success();
    }
}
