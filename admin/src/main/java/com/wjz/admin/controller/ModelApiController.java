package com.wjz.admin.controller;

import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.entity.bo.BasePage;
import com.wjz.commons.entity.vo.ModelVo;
import com.wjz.commons.service.ModelApiService;
import com.wjz.commons.util.ResultTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author wjz
 * @date 2021/7/7 15:20
 */
@RestController
@RequestMapping("/model")
public class ModelApiController {

    @Autowired
    private ModelApiService modelApiService;

    @GetMapping("/queryModelList")
    public AjaxResult queryModelList(BasePage basePage, String label) {
        return ResultTool.success(modelApiService.queryModelList(basePage, label));
    }

    @PostMapping("/addModel")
    public AjaxResult addModel(@RequestBody ModelVo modelVo) {

        return modelApiService.addModel(modelVo);
    }

    @DeleteMapping("/{objectId}")
    public AjaxResult deleteModel(@PathVariable Long objectId) {
        modelApiService.deleteModel(objectId);
        return ResultTool.success();
    }

    @PutMapping
    public AjaxResult update(@RequestBody ModelVo modelVo) {
        modelApiService.update(modelVo);
        return ResultTool.success();
    }

    @PutMapping("/rename")
    public AjaxResult rename(@RequestBody ModelVo modelVo) {
        return modelApiService.rename(modelVo);
    }
}
