package com.wjz.admin.config.security.handle;

import com.alibaba.fastjson.JSON;
import com.wjz.commons.entity.bo.AjaxResult;
import com.wjz.commons.util.ResultTool;
import com.wjz.admin.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * @author wjz
 * @date 2020/11/24 17:11
 * @Description 登录成功处理逻辑
 */
@Component
public class CustomizeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private SecurityUtils securityUtils;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        AjaxResult result = ResultTool.success(new HashMap() {{
//            put("token", Base64Utils.encodeToString(httpServletRequest.getSession().getId().getBytes()));
            put("user", securityUtils.getSimplenessCurrentUser());
            put("roles", securityUtils.getAuthentication().getAuthorities());
        }});
        httpServletResponse.setContentType("text/json;charset=utf-8");
        httpServletResponse.getWriter().write(JSON.toJSONString(result));
    }
}
